from django.shortcuts import render, redirect
from django.http import HttpResponse
from todolist_app.models import TaskList
from todolist_app.form import Taskform
from django.contrib import messages
# Create your views here.


def todolist(request):
    if request.method == "POST":
        form = Taskform(request.POST or None)
        if form.is_valid():
            form.save()
        messages.success(request, ('New task added!'))
        return redirect('todolist')
    else:
        alltask = TaskList.objects.all
        context = {
            'alltask': alltask
        }
        return render(request, 'todolist.html', context)

# def todolist(request):
#     context = {
#         'todolist':'welcome to todo list '
#     }
#     return render(request,'todolist.html',context)


def contact(request):
    context = {
        'contact_text': 'welcome to contact '
    }
    return render(request, 'contact.html', context)


def about(request):
    context = {
        'about_text': 'welcome to about '
    }
    return render(request, 'about.html', context)
